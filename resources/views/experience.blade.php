@extends('layouts.main')

@section('container')
<section class="resume-section" id="experience">
    <div class="resume-section-content">
        <h2 class="mb-5">Experience</h2>
        <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
            <div class="flex-grow-1">
                <h3 class="mb-0">Anggota Sie Pubdok</h3>
                <div class="subheading mb-3">TWEAK #4</div>
                <p>Lorem ipsum, dolor sit amet consectetur adipisicing elit. Qui, soluta voluptate repudiandae laborum architecto illo, id blanditiis dolorem debitis magni odio omnis enim tenetur mollitia veritatis deserunt amet officia ex similique nihil exercitationem excepturi atque? Error quaerat, perspiciatis consequatur nesciunt officiis sequi ea voluptatibus! Nobis nam et reprehenderit officiis qui, reiciendis, eaque explicabo sint quas ducimus cumque? Nostrum, veniam iure eligendi ipsum accusamus velit vitae. Aspernatur cum quae quam similique totam, modi rerum voluptas consequuntur at temporibus voluptate sapiente voluptatum?</p>
            </div>
            <div class="flex-shrink-0"><span class="text-primary">November 2020 - Present</span></div>
        </div>
        <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
            <div class="flex-grow-1">
                <h3 class="mb-0">Anggota Sie Pubdok</h3>
                <div class="subheading mb-3">Integer #2</div>
                <p>Lorem ipsum dolor sit amet consectetur, adipisicing elit. Quas illum modi esse rerum at beatae officiis ipsam eaque voluptates expedita vel facere porro, minima saepe eveniet nesciunt sapiente odit recusandae id error dolor asperiores delectus optio. Sunt, quaerat placeat id a rem doloribus possimus soluta at maiores iure autem. Temporibus aliquid eius dolorum vero deserunt velit fugiat minima voluptates cumque veniam aspernatur itaque atque a expedita eum numquam autem nulla, soluta, commodi quod nemo sunt! Explicabo, facilis sint. Laboriosam cumque esse maxime ad dolorem? Minima iste optio ea ipsam modi? Officia atque iure labore quas praesentium quod ipsum in.</p>
            </div>
            <div class="flex-shrink-0"><span class="text-primary">September 2020 - OKtober 2020</span></div>
        </div>
        <div class="d-flex flex-column flex-md-row justify-content-between mb-5">
            <div class="flex-grow-1">
                <h3 class="mb-0">UI/UX Project</h3>
                <div class="subheading mb-3">Shout! Media Productions</div>
                <p>Podcasting operational change management inside of workflows to establish a framework. Taking seamless key performance indicators offline to maximise the long tail. Keeping your eye on the ball while performing a deep dive on the start-up mentality to derive convergence on cross-platform integration.</p>
            </div>
            <div class="flex-shrink-0"><span class="text-primary">September 2021 - OKtober 2021</span></div>
        </div>
    </div>
</section>
@endsection