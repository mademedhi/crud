@extends('layouts.main')

@section('container')
<section class="resume-section" id="about">
    <div class="resume-section-content">
        <h1 class="mb-0">
            Medhi
            <span class="text-primary">Permana</span>
        </h1>
        <div class="subheading mb-5">
            44 Gajah Mada Street · Singaraja, CO 81112 · +62 81593784234 ·
            <a href="mailto:name@email.com">medhipermana@email.com</a>
        </div>
        <p class="lead mb-5">Saya berpengalaman dalam memanfaatkan kerangka kerja tangkas untuk memberikan sinopsis yang kuat untuk ikhtisar tingkat tinggi. Pendekatan berulang untuk strategi perusahaan mendorong pemikiran kolaboratif untuk memajukan proposisi nilai secara keseluruhan.</p>
        <div class="social-icons">
            <a class="social-icon" target="_blank" href="https://gitlab.com/medhipermana"><i class="fab fa-gitlab"></i></a>
            <a class="social-icon" target="_blank" href="https://www.instagram.com/medhi.permana"><i class="fab fa-instagram"></i></a>
            <a class="social-icon" target="_blank" href="https://www.facebook.com/medhi.permana/"><i class="fab fa-facebook-f"></i></a>
        </div>
    </div>
</section>
@endsection